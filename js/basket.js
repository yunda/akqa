function ajaxRequest(params){
    var request = new XMLHttpRequest();

    request.onreadystatechange = function() {
        if ( request.readyState === 4 ) {
            if ( request.status === 200 ) {
                !params.success || params.success(request.responseText, request);
            } else {
                !params.errorHandler || params.errorHandler(request);
            }
        }
    }
    request.open(params.type || 'GET', params.url, 'async' in params ? params.async : true);
    if (params.type === 'POST') {
        request.setRequestHeader('Content-type', params.contentType || 'application/json;charset=UTF-8');
        request.send(params.data);
    } else {
        request.send();
    }
    
}

function CartItem (id, name, size, price, quantity){
    this.id = id;
    this.name = name;
    this.size = size;
    this.price = price;
    this.quantity = ko.observable(quantity);
    this.cost = ko.computed(function (){
        return (this.price * this.quantity()).toFixed(2) - 0;
    }, this);

    this.quantityKeyDown = function (self, e) {
        var key = e.keyCode,
            keychar = String.fromCharCode(key);

        if ( ~("0123456789").indexOf(keychar) || ~([96,97,98,99,100,101,102,103,104,105]).indexOf(key) ) {
            return true;
        } else if ( ~([37,39,8,13,46,27,9]).indexOf(key) ){
            return true;
        } else {
            return false;
        }
    };

    this.quantityKeyUp = function (self, e) {
        if ( (self.quantity() - 0) > 10 ) {
            alert('The product quantity can\'t be more than a 10');
            self.quantity(10)
        }
    }

    this.addOne = function (self, e) {
        var newQty = self.quantity() - 0 + 1;
        if ( newQty <= 10 ) {
            self.quantity(newQty);
        }
    };

    this.subtractOne = function (self, e) {
        var newQty = self.quantity() - 1;
        if ( newQty >= 0 ) {
            self.quantity(newQty);
        }
    };

}

function CartViewModel() {
    var self = this;

    // Editable data
    self.cartItems = ko.observableArray([]);

    // This data supposed to come from the server
    self._cartData = [
        { id: 1, name: "Cotton T-Shirt", size: "Medium", price: 1.99, quantity: 1 },
        { id: 2, name: "Baseball Cap", size: "One Size", price: 2.99, quantity: 2 },
        { id: 3, name: "Swim Shorts", size: "Medium", price: 3.99, quantity: 1 }
    ];

    self._cartData.forEach(function (item, i){
        self.cartItems.push(new CartItem(item.id, item.name, item.size, item.price, item.quantity));
    }, self);

    // ajaxRequest({
    //     type: 'GET',
    //     url: 'data.json',
    //     success: function (data) {
    //         self._cartData = JSON.parse(data);
    //         self._cartData.forEach(function (item, i){
    //             self.cartItems.push(new CartItem(item.id, item.name, item.size, item.price, item.quantity));
    //         }, self);
    //     },
    //     errorHandler: function (req) {
    //         alert(req.statusText);
    //     }
    // });
    

    self.subtotal = ko.computed(function () {
        var sum = 0;
        self.cartItems().forEach(function (item, i) {
            sum += item.cost();
        });
        return sum.toFixed(2) - 0;
    }, self);

    self.vat = ko.computed(function () {
        return (self.subtotal() * 0.2).toFixed(2) - 0;
    }, self);

    self.total = ko.computed(function () {
        return (self.subtotal() + self.vat()).toFixed(2) - 0;
    }, self);

    self.removeItem = function (item) {
        self.cartItems.remove(item);
        // request with new cart items collection
        ajaxRequest({
            type: 'POST',
            url: '/submitURL',
            data: ko.toJSON(self.cartItems),
            async: true
        });
    };

    self.submit = function (form) {
        var JSONstring = ko.toJSON({ items: self.cartItems, subtotal: self.subtotal(), vat: self.vat(), total: self.total() });
        
        console.log( JSONstring );

        ajaxRequest({
            type: 'POST',
            url: form.attributes.action,
            data: JSONstring,
            async: false
        });
        alert('Your basket has been submited! Check console to see the JSON');
    }
    
}

ko.applyBindings(new CartViewModel());